﻿using System;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace xmlfusion
{
    class Program
    {
        public static void Main(string[] args)
        {
            ouvertureDuRepertoire:
            Console.WriteLine("Entrez le chemin du dossier contenant les fichiers");
            string chemin = Console.ReadLine();
            var xmlSortie = new XDocument();
            try
            {
                xmlSortie = XDocument.Load(Directory.EnumerateFiles(chemin, "*.xml").First());
            }
            catch (System.IO.DirectoryNotFoundException directoryNotFound)
            {
                Console.WriteLine("Répertoire introuvable");
                goto ouvertureDuRepertoire;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Impossible d'accéder au répertoire");
                goto ouvertureDuRepertoire;
            }
            if (xmlSortie != null)
            {
                XPathNavigator informations = xmlSortie.CreateNavigator();
                informations.MoveToFollowing(XPathNodeType.Element);
                XNamespace ns = informations.NamespaceURI;
                foreach (string item in Directory.EnumerateFiles(chemin, "*.xml").Skip(1))
                {
                    XDocument xml = XDocument.Load(item);
                    xmlSortie.Descendants(ns + "detailsTrafic").FirstOrDefault().Add(xml.Descendants(ns + "detailsTrafic").FirstOrDefault().Nodes());
                }
                creationDuFichier:
                Console.WriteLine("Entrez un nom pour le fichier");
                string nomDuFichier = Console.ReadLine();
                if (!nomDuFichier.EndsWith(".xml"))
                {
                    nomDuFichier += ".xml";
                }
                try
                {
                    xmlSortie.Save(nomDuFichier);
                }
                catch
                {
                    Console.WriteLine("Nom incorrect. \nNe pas mettre de '\\' ou  ne pas utiliser des noms interdits peut résoudre cette erreur ");
                    goto creationDuFichier;
                }
                    
            }
            else
            {
                Console.WriteLine("Aucun fichier xml dans ce répertoire : " + chemin);
            }                      
        }
    }
}